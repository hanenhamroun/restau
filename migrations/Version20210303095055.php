<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210303095055 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category ADD image VARCHAR(255) NOT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE createdby_id createdby_id INT DEFAULT NULL, CHANGE createdat createdat DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE plat ADD createdat DATE DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE restau_id restau_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE restau CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD avatar VARCHAR(255) NOT NULL, CHANGE salt salt VARCHAR(255) DEFAULT NULL, CHANGE last_login last_login DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT NULL, CHANGE password_requested_at password_requested_at DATETIME DEFAULT NULL, CHANGE firstname firstname VARCHAR(255) DEFAULT NULL, CHANGE lastname lastname VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP image, CHANGE category_id category_id INT DEFAULT NULL, CHANGE createdby_id createdby_id INT DEFAULT NULL, CHANGE createdat createdat DATE DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE plat DROP createdat, CHANGE category_id category_id INT DEFAULT NULL, CHANGE restau_id restau_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE restau CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP avatar, CHANGE salt salt VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_login last_login DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE password_requested_at password_requested_at DATETIME DEFAULT \'NULL\', CHANGE firstname firstname VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE telephone telephone VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}
