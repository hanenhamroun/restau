$(document).on('click', '.changePagination', function () {
    var nb = $(this).attr('nb');
     
    $.ajax({
        type: "POST",
        url: Routing.generate('paginate_categories'),
        cache: false,
        data: {nb: nb },
        success: function (data) {
            $('.render_categ').html(data);
        }
    }); 
});