<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Filemanager {

    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;
    }
public function validEmail($email) {

        $domain = substr(strrchr($email, "@"), 1);
        $mxHosts = [];
        dns_get_mx($domain, $mxHosts);
        $response = '1';
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response = '0';
        } elseif (!count($mxHosts)) {

            $response = '0';
        }
        return $response;
    }
    public function uploadfile($file, $namefolder = null) {
        $tmp = $file['tmp_name'];

        if ($namefolder == null) {
            $dir_root = __DIR__ . '/../../public/images/';
        } else {
            $dir_root = __DIR__ . '/../../public/' . $namefolder . '/';
        }

        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $name_file = rand(0, 1000000) . '.' . $ext;
        $extension = strtolower($ext);
        move_uploaded_file($tmp, $dir_root . $name_file);
        return [$name_file, $extension];
    }

}
