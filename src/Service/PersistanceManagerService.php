<?php

namespace App\Service;
 
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as container;
use Symfony\Component\PropertyAccess\PropertyAccess;

class PersistanceManagerService {

    private $em; 

    public function __construct(EntityManagerInterface $em) {

        $this->em =$em; 
    }

    //METHOD SAVE ENTITY
    public function Save($entity) {

        $this->em->persist($entity);
        $this->em->flush();
    }

    //METODH TO SET FIELDS OBJECT BY PROPERTYACCESS FUNCTION 
    public function Savebyfields($entity, $fields) {

        foreach ($fields as $method => $value) {
            $accessor = PropertyAccess::createPropertyAccessor();
            $accessor->setValue($entity, $method, $value);
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    //METHOD REMOVE ENTITY
    public function Remove($entity) {

        $this->em->remove($entity);
        $this->em->flush();
    }

}
