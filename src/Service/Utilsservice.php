<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Utilsservice {

    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;
    }

    public function validEmail($email) {

        $domain = substr(strrchr($email, "@"), 1);
        $mxHosts = [];
        dns_get_mx($domain, $mxHosts);
        $response = '1';
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response = '0';
        } elseif (!count($mxHosts)) {

            $response = '0';
        }
        return $response;
    }

}
