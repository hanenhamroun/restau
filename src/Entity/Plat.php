<?php

namespace App\Entity;

use App\Repository\PlatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlatRepository::class)
 */
class Plat {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity="Category",cascade={"remove"})  
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Restau",cascade={"remove"})  
     */
    private $restau;

    /**
     * @var \Date
     *
     * @ORM\Column(name="createdat", type="date",nullable=true)
     */
    private $createdat;

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function setPrix(float $prix) {
        $this->prix = $prix;

        return $this;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    public function getRestau() {
        return $this->restau;
    }

    public function setRestau($restau) {
        $this->restau = $restau;

        return $this;
    }

    public function getCreatedat() 
    {
        return $this->createdat;
    }

    public function setCreatedat( $createdat) 
    {
        $this->createdat = $createdat;

        return $this;
    }

}
