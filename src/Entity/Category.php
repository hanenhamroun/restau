<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Category",cascade={"remove"})  
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="User")  
     */
    private $createdby;

    /**
     * @var \Date
     *
     * @ORM\Column(name="createdat", type="date",nullable=true)
     */
    private $createdat;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $image;

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    public function getCreatedby() {
        return $this->createdby;
    }

    public function setCreatedby($createdby) {
        $this->createdby = $createdby;

        return $this;
    }

    public function getCreatedat() {
        return $this->createdat;
    }

    public function setCreatedat($createdat) {
        $this->createdat = $createdat;

        return $this;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

}
