<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DashController extends AbstractController {

   

    /**
     * @Route("/displayparentcategories", name="displayparentcategories")
     */
    public function displayparentcategories() {

        $em = $this->getDoctrine();
        $categories = $em->getRepository('App:Category')->findBy(["category" => NULL]);
        return $this->render('dash/parentcategories.html.twig', [ 'categories' => $categories]);
    }

    /**
     * @Route("/", name="dashboard")
     */
    public function dahsboard() {
        $em = $this->getDoctrine();
        $restaurants = $em->getRepository('App:Restau')->findAll();
        $categories = $em->getRepository('App:Category')->findBy(["category" => NULL]);
        return $this->render('dash/index.html.twig', [ 'restaurants' => $restaurants, 'categories' => $categories]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact() {
        return $this->render('dash/contact.html.twig', []);
    }

    /**
     * @Route("/search/plat", name="search_plat")
     */
    public function search_plat(Request $request) {

        $em = $this->getDoctrine();
        $plats = $em->getRepository('App:Plat')->findbynom($request->get('platname'));
        return $this->render('dash/resultsearchplat.html.twig', ["plats" => $plats, "search" => $request->get('platname')]);
    }

}
