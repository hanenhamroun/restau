<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/dash", name="dashadmin")
     */
    public function index() {
        return $this->render('admin/admin.html.twig', [ 'controller_name' => 'DashController']);
    }
    
     /**
     * @Route("/admin/restau", name="adminrestau")
     */
    public function adminrestau() {
        $em = $this->getDoctrine();
        $restaux = $em->getRepository('App:Restau')->findAll();
        return $this->render('restau/crud.html.twig', [
                    'restaux' => $restaux,
        ]);
    }
}
