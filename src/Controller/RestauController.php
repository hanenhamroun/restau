<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Restau;
use App\Service\PersistanceManagerService;
use App\Service\Filemanager;
use App\Service\Utilsservice;

class RestauController extends AbstractController {

    private $persistmanager;
    private $filemanager;
    private $utilsservice;

    public function __construct(PersistanceManagerService $persistmanager, Filemanager $filemanager, Utilsservice $utilsservice) {
        $this->persistmanager = $persistmanager;
        $this->filemanager = $filemanager;
        $this->utilsservice = $utilsservice;
    }

    // index espace restau

    /**
     * @Route("/customer/index", name="dash_customer")
     */
    public function customerindex() {
        return $this->render('restau/dash.html.twig', []);
    }

    // index espace profess
    /**
     * @Route("/espace/index", name="index_espace")
     */
    public function indexespace() {
        return $this->render('restau/index.html.twig', []);
    }

    /**
     * @Route("/validate/customer/inscri", name="validation_inscri")
     */
    public function validateinscri(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $existuser = $em->getRepository("App:User")->findOneBy(["email" => $request->get('email')]);
        if ($existuser != null) {
            $this->persistmanager->Savebyfields($existuser, ["enabled" => 1]);
            return new Response('OK');
        }
        return new Response('NO');
    }

    /**
     * @Route("/add/customer", name="add_customer")
     */
    public function addcustomer(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $phone = $request->get('phone');
        $email = $request->get('email');
        $desc = $request->get('desc');
        $password = $request->get('password');
        $name = $request->get('restau');
        if ($request->getMethod() == "POST") {

            if ($_FILES['logo']['name'] == "" || trim($name) == "" || trim($firstname) == "" || trim($password) == "" || trim($lastname) == "" || trim($phone) == "" || trim($email) == "") {
                $this->addFlash('error', "Tous les champs sont obligatoire.");
                return $this->redirect($this->generateUrl('add_customer', ["firstname" => $firstname, "lastname" => $lastname, "phone" => $phone,
                                    "email" => $email, "name" => $name, "desc" => $desc]));
            }


            $response = $this->utilsservice->validEmail($email);
            if ($response == "0") {
                $this->addFlash('error', "Veuillez saisir un email valid.");
                return $this->redirect($this->generateUrl('add_customer', ["firstname" => $firstname, "lastname" => $lastname, "phone" => $phone,
                                    "email" => $email, "name" => $name, "desc" => $desc]));
            }

            $existuser = $em->getRepository("App:User")->findOneBy(["email" => $email]);
            if ($existuser != null) {
                $this->addFlash('error', "Email existe Déjà.");
                return $this->redirect($this->generateUrl('add_customer', ["firstname" => $firstname, "lastname" => $lastname, "phone" => $phone,
                                    "email" => $email, "name" => $name, "desc" => $desc]));
            }


            $attch = "";
            $user = new User();
            $restau = new Restau();
            $user->addRole('ROLE_RESTAU');
            $user->setPlainPassword($password);
            $user->setUsername("mh@" . $phone);
            $this->persistmanager->Savebyfields($user, [ "avatar" => strtolower(substr($firstname, 0, 1)) . ".jpg",
                "telephone" => $phone, "email" => $email, "firstname" => $firstname, "lastname" => $lastname]);
            $this->persistmanager->Savebyfields($restau, ["logo" => $attch, "description" => $desc,
                "name" => $name,
                "user" => $user]);

            if ($_FILES['logo']['name'] != "") {
                $file = $_FILES['logo'];
                $result = $this->filemanager->uploadfile($file, 'images/restau');
                $attch = $result[0];
                $this->persistmanager->Savebyfields($restau, ["logo" => $attch]);
            }
            $this->addFlash('Success', "Merci pour votre inscription. Veuillez consulter votre boite email pour valider votre compte");

            //send mail validation 
        }


        return $this->render('restau/add.html.twig', ["firstname" => $firstname, "lastname" => $lastname, "phone" => $phone,
                    "email" => $email, "name" => $name, "desc" => $desc]);
    }

}
