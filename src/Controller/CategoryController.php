<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Category;
use App\Service\PersistanceManagerService;
use App\Service\Filemanager;

class CategoryController extends AbstractController {

    private $persistmanager;
    private $filemanager;

    public function __construct(PersistanceManagerService $persistmanager, Filemanager $filemanager) {
        $this->persistmanager = $persistmanager;
        $this->filemanager = $filemanager;
    }

    /**
     * @Route("/admin/category", name="admincategory")
     * @Route("/customer/category", name="customercategory")
     */
    public function categoryindex(Request $request, PaginatorInterface $paginator) {
        $em = $this->getDoctrine();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $categories = $em->getRepository('App:Category')->findAll();
            $categs = $em->getRepository('App:Category')->findBy(["category" => NULL]);
        } else {
            $categories = $em->getRepository('App:Category')->findBy(["createdby" => $this->getUser()->getId()]);
            $categs = $em->getRepository('App:Category')->findBy(["category" => NULL, "createdby" => $this->getUser()->getId()]);
        }
        $pagination = $paginator->paginate($categories, $request->query->getInt('page', 1), 2);
        return $this->render('category/index.html.twig', [
                    'categories' => $pagination,
                    'categs' => $categs
        ]);
    }

    /**
     * @Route("/paginate_categories", name="paginate_categories", options={"expose"=true})
     */
    public function paginate_categories(Request $request, PaginatorInterface $paginator) {
        $em = $this->getDoctrine();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN'))
            $categories = $em->getRepository('App:Category')->findAll();
        else
            $categories = $em->getRepository('App:Category')->findBy(["createdby" => $this->getUser()->getId()]);
        $pagination = $paginator->paginate($categories, $request->query->getInt('page', $request->get('nb')), 2);
        return $this->render('category/renderlistcategories.html.twig', [
                    'categories' => $pagination,
        ]);
    }

    /**
     * @Route("/add/category", name="addcategory", options={"expose"=true})
     */
    public function addcategory(Request $request) {
        $em = $this->getDoctrine();
        if ($request->getMethod() == "POST") {
            if ($request->get('nom') != "") {
                $category = new Category();
                $parent = null;
                if ($request->get('parent') != "0") {
                    $parent = $em->getRepository('App:Category')->find($request->get('parent'));
                }

                $this->persistmanager->Savebyfields($category, ["category" => $parent, "name" => $request->get('nom'), "createdby" => $this->getUser(), "createdat" => new \DateTime()]);

                if ($_FILES['imagecateg']['name'] != "") {
                    $file = $_FILES['imagecateg'];
                    $result = $this->filemanager->uploadfile($file, 'images/categories');
                    $attch = $result[0];
                    $this->persistmanager->Savebyfields($category, ["image" => $attch]);
                }
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN'))
                return $this->redirect($this->generateUrl('admincategory'));
            else
                return $this->redirect($this->generateUrl('customercategory'));
        }

        return new Response("");
    }

    /**
     * @Route("/displaychildscategories", name="displaychildscategories", options={"expose"=true})
     */
    public function displaychildscategories($parent) {
        $em = $this->getDoctrine();
        $categparent = $em->getRepository('App:Category')->find($parent);
        $childs = $em->getRepository('App:Category')->findBy(["category" => $categparent]);
        return $this->render('category/displaychildscategories.html.twig', [
                    'childs' => $childs,
        ]);
    }

    /**
     * @Route("/haschild", name="haschild", options={"expose"=true})
     */
    public function haschild($categ) {
        $em = $this->getDoctrine();
        $categparent = $em->getRepository('App:Category')->find($categ);
        $childs = $em->getRepository('App:Category')->findBy(["category" => $categparent]);
        if (count($childs) == 0)
            return new Response('0');
        return new Response('1');
    }

}
