<?php

namespace App\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RedirectResponse; 

class UserRedirection implements AuthenticationSuccessHandlerInterface {

    private $router; 

    public function __construct(RouterInterface $router ) {
        $this->router = $router; 
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

        $redirection = new RedirectResponse($this->router->generate('dashboard'));
        if ($token->getUser()->getRoles()[0] == 'ROLE_SUPER_ADMIN') {
            $redirection = new RedirectResponse($this->router->generate('dashadmin'));
        } elseif ($token->getUser()->getRoles()[0] == 'ROLE_RESTAU') {
            $redirection = new RedirectResponse($this->router->generate('dash_customer'));
        }
        return $redirection;
    }

}
